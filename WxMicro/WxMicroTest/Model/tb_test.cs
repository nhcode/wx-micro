﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using WxUtile.Attributes;
using WxUtile.Const;

namespace WxMicroTest.Model
{
    [Explain("测试数据表")]
    [DataContract(Name = "tb_test")]
    public class tb_test
    {
        [Key]
        [NotNull]
        [Explain("数据编号")]
        [DataMember(Name ="ID")]
        [SQL(WxDataType.d_varchar_50)]
        public string ID { get; set; }

        [NotNull]
        [Explain("名称")]
        [DataMember(Name ="Name")]
        [SQL(WxDataType.d_varchar_128)]
        public string Name { get; set; }

        [Explain("证件号")]
        [DataMember(Name = "IDNumber")]
        [SQL(WxDataType.d_varchar_50)]
        public string IDNumber { get; set; }

        [Explain("测试")]
        [DataMember(Name = "test")]
        [SQL(WxDataType.d_varchar_20)]
        public string test { get; set; }
    }
}
