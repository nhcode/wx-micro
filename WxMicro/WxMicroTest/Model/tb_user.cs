﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using WxUtile.Attributes;
using WxUtile.Interfaces;
using WxUtile.Const;

namespace WxMicroTest.Model
{
    [Explain("用户表")]
    [DataContract(Name = "tb_user")]
    public class tb_user : IUser
    {
        [Key]
        [NotNull]
        [Explain("用户编号")]
        [DataMember(Name = "ID")]
        [SQL(WxDataType.d_varchar_50)]
        public override string ID { get; set; }

        [Key]
        [NotNull]
        [Explain("用户账号")]
        [DataMember(Name = "UserName")]
        [SQL(WxDataType.d_varchar_128)]
        public override string UserName { get; set; }

        [DataMember(Name = "OtherInformation")]
        protected override string OtherInformation { get; set; }

        [DataMember(Name = "UserPublicKey")]
        public override string UserPublicKey { get; set; }

        public override List<string> GetAuthPortIDArray()
        {
            return new List<string>();
        }
    }
}
