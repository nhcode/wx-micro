﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WxUtile;
using WxUtile.Interfaces;
using WxUtile.Models;
using WxMicroTest.Consts;

namespace WxMicroTest
{
    public class ConfigStartup : Startup, WxUtile.Interfaces.IStartup, IAuthorizService
    {
        public ConfigStartup(IConfiguration configuration)
        {
            this.initialize(configuration, this);
        }

        public WxError AccessError()
        {
            return new WxError()
            {
                APIErroUrl = $"/test/Error/ApiErro",
                PageErroUrl = $"/test/Error/WebErro"
            };
        }

        /// <summary>
        /// 用户证书失效时间，单位分钟
        /// </summary> 
        /// <returns></returns>
        public long CertificateExpirationTime()
        {
            return 1L;
        }

        /// <summary>
        /// 获取授权所需AuthorizationService对象
        /// </summary>
        /// <returns></returns>
        public IAuthorizService GetAuthorizService()
        {
            return this;
        }
      
        /// <summary>
        /// 网站资源允许正则
        /// </summary>
        /// <returns></returns>
        public string GetPublicResourceTypes()
        {
            return WxMicroTestConfig.PublicResourceTypes;
        }

        /// <summary> 
        /// 缓存redis服务连接地址
        /// </summary>
        /// <returns></returns>
        public string GetRedisConnecting()
        {
            return WxMicroTestConfig.RedisServiceUrl;
        }

        /// <summary>
        ///获取平台所需要的相关资料
        /// </summary>
        /// <returns></returns>
        public WxTheISsuedBy GetTheISsuedBy() => WxMicroTestConfig.TheISsuedBy;

        /// <summary>
        /// 数据可初始化
        /// </summary>
        public void Initialize()
        {

        }

        /// <summary>
        /// 消息签名有效时间
        /// </summary>
        /// <returns></returns>
        public long MsgMinutes()
        {
            return 2;
        }

        /// <summary>
        /// 推送用户至该方法
        /// </summary>
        /// <param name="user"></param>
        public void ToUser(IUser user)
        {
            ///用户访问请求时该方法可以获取到用户资料
            return;
        }

       
    }
}
