﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WxUtile.Strings.Encrypts;

namespace WxMicroTest.Consts
{
    public class RouteConst
    {
        public const string RouteValue ="WxMicroTest/[controller]/[action]";
        public const string RouteValueForToken = RouteValue + "/{Token?}";
    }
}
