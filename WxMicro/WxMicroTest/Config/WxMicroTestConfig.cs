﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WxUtile.Configs;
using WxUtile.DataBase;
using WxUtile.Models;

namespace WxMicroTest.Consts
{
    public class WxMicroTestConfig
    {
        /// <summary>
        /// 配置地址
        /// </summary>
        private static string CONFIG_PAHT = $"Config/{typeof(WxMicroTestConfig).Name}.json";

        /// <summary>
        /// 配置项初始化
        /// </summary>
        public static WxUtileConfig Config => WxUtileConfig.GetConfig(CONFIG_PAHT);

        /// <summary>
        /// 服务器缓存链接地址
        /// </summary>
        public static string RedisServiceUrl = Config.GetValue("RedisServiceUrl");
        /// <summary>
        /// 数据库连接对象
        /// </summary>
        public static WxDbData DbServiceUrl = Config.GetOBject<WxDbData>("DbServiceUrl");

        /// <summary>
        /// 平台资料参数
        /// </summary>
        public static WxTheISsuedBy TheISsuedBy = Config.GetOBject<WxTheISsuedBy>("TheISsuedBy");

        /// <summary>
        /// 系统名称
        /// </summary>
        public static string SystemName = Config.GetValue("SystemName");

        /// <summary>
        /// 网站资源允许正则
        /// </summary>
        /// <returns></returns>
        public static string PublicResourceTypes = Config.GetValue("PublicResourceTypes");
    }
}
