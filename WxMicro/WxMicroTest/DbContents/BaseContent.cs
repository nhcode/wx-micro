﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WxUtile.DataBase;
using WxMicroTest.Consts;
using WxMicroTest.Model;

namespace WxMicroTest.DbContents
{
    public class BaseContent : WxBaseContent
    {
        public override WxDbData GetDbData() => WxMicroTestConfig.DbServiceUrl;

        public DbSet<tb_test> tb_test { get; set; }
        public DbSet<tb_user> tb_user { get; set; }

        /// <summary>
        /// 模板操作映射对象 
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<tb_test>().ToTable("tb_test").HasKey(o => o.ID);

            modelBuilder.Entity<tb_user>().ToTable("tb_user").HasKey(o => o.ID);
        }

    }
}
