﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WxUtile.Attributes;
using WxUtile.Controllers;
using WxUtile.Interfaces;
using WxUtile.Models.Ports;
using WxUtile.Data;
using WxUtile.Enums;
using WxUtile.Strings.Encrypts;
using WxMicroTest.Consts;
using WxMicroTest.Model;
using WxUtile.Requests;
using WxUtile.Strings;
using static System.Net.WebRequestMethods;
using Microsoft.AspNetCore.Http;
using System.Net;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using WxMicro;

namespace WxMicroTest.Controllers
{
    [Route(RouteConst.RouteValueForToken)]
    [PortService(typeof(testController))]
    public class testController : BaseController
    {
        [HttpGet]
        [Api("123456")]
        public IActionResult Index()
        {
            HttpContext.GetIP();
            return View();
        }

        [HttpGet]
        [Api("测试收益权2")]
        public IActionResult Rsa()
        {
            var item = new tb_user { ID = "123123123", UserName = "username" };
            item.SetOtherInformation(new tb_test
            {
                ID="123", 
                IDNumber="123",
                test=""
            });
            return WxMsg.Set(true, "数据请求成功", WxEMsg.OK,Certificate(item));
             
        }

        [HttpGet]
        [Api("获取接口列表")]
        [Menu("设计->接口列表", 1)]
        [Remark("测试接口")]
        public IActionResult Test()
        {
            return WxMsg.Set(true, "查询成功", WxEMsg.OK,GetWxPorts());
        }

        [HttpGet]
        [Api("加载模块组件")]
        [Remark("模块加载")]
        public IActionResult AddLib([FromServices] ApplicationPartManager manager,[FromServices] DynamicChangeTokenProvider dynamic)
        {
            foreach(var item in dynamic.InitializeAssembly())
            {
                manager.ApplicationParts.Add(new AssemblyPart(item));
                dynamic.NotifyChanges();
            }
            return WxMsg.Set(true, "新模块加载成功", WxEMsg.OK, GetWxPorts());
        }
        [HttpGet]
        [Api("获取接口列表")]
        [Remark("获取接口列表")]
        public object GetApis()
        {
            return this.GetWxPorts();
        }

        //发送使用案例
        [HttpGet]
        [Api("发送使用案例")]
        public void SendMsg()
        {
            HttpContext.SendMsg("你好");
            
        }
        //发送使用案例

        [HttpGet]
        [Api("发送使用案例")]
        public void SendMsgs()
        {
            HttpContext.SendMsg(202, "你好");
        }

        //获取请求头与获取客户端IP案例

        [HttpGet]
        [Api("获取请求头与获取客户端IP案例")]
        public async Task SetGet()
        {
            QueryString HttpC;
            var items = HttpContext.Request.Get<object>(ref HttpC);
            var IP = HttpContext.GetIP();

            var data= items.ToJson();

           //await HttpContext.SendMsg(items);

           await HttpContext.SendMsg(IP);

           await HttpContext.SendMsg(data);

            //get网络请求
            var GetData = WxHttp.HttpGet("https://www.test.com.cn/teste/test/index");

            CookieContainer cookieContainer=new CookieContainer();
            var GetData2 = WxHttp.HttpGet("https://www.test.com.cn/teste/test/index",ref cookieContainer);
            
            //POST网络请求
            var PostData = WxHttp.HttpPost("https://www.test.com.cn/teste/test/index","a=1&b=12");

            var PostData2 = WxHttp.HttpPost("https://www.test.com.cn/teste/test/index", ref cookieContainer, "a=1&b=12");



        }



    }
}
