
#特性对象
 
## [PortServiceAttribute.class](PortServiceAttribute.html)  接口服务特性对象
## [RemarkAttribute.class](RemarkAttribute.html)  备注特性，提供程序员备注对应接口的文档资料
## [AuthorizationAttribute.class](AuthorizationAttribute.html)  用户授权（需要用户登录并获得访问允许的相应权限才能访问）
## [UserDataAttribute.class](UserDataAttribute.html)  用户资料（需要使用用户资料，但不需要用户获得授权就可以访问）
## [SignDataAttribute.class](SignDataAttribute.html)  标识 用户请求接口时需要对资料进行签名处理
## [ApiAttribute.class](ApiAttribute.html)  标识接口是API
## [PageAttribute.class](PageAttribute.html)  标识接口是Page
## [MenuAttribute.class](MenuAttribute.html)  标识接口为菜单
## [WxMenuGroup.class](WxMenuGroup.html)  菜单组对象
## [ExplainAttribute.class](ExplainAttribute.html)  属性解释标识 
## [NotNullAttribute.class](NotNullAttribute.html)  注释程序属性不允许为空值
## [SQLAttribute.class](SQLAttribute.html)  标识某属性为数据表字段
 