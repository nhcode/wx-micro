**[返回](Attribute.html "返回")**
# ExplainAttribute.class #

**介绍**

属性解释

属性解释标识
**继承**

Attribute.class 

**构造函数** 

| 名称| 说明 |
| ---- | ---- | 
|ExplainAttribute(string Value)|
|无|
 **属性**

|类型|名称|说明|
|----|----|----|
|string|Value|解释值|
