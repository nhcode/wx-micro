**[返回](Attribute.html "返回")**
# SignDataAttribute.class #

**介绍**

标识 用户请求接口时需要对资料进行签名处理


**继承**

Attribute.class 

**构造函数** 

| 名称| 说明 |
| ---- | ---- | 
|无|
 **属性**
