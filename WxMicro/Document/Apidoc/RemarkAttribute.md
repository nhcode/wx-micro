**[返回](Attribute.html "返回")**
# RemarkAttribute.class #

**介绍**

备注特性，提供程序员备注对应接口的文档资料


**继承**

Attribute.class 

**构造函数** 

| 名称| 说明 |
| ---- | ---- | 
|RemarkAttribute(string Remark)|备注特性标注构造函数|

 **属性**

| 类型 | 名称 |  说明 | 
| ---- | ---- |-----|
|string|Remark|备注|
