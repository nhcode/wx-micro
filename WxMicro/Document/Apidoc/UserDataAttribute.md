**[返回](Attribute.html "返回")**
# UserDataAttribute.class #

**介绍**

用户资料（需要使用用户资料，但不需要用户获得授权就可以访问）


**继承**

Attribute.class 

**构造函数** 

| 名称| 说明 |
| ---- | ---- | 
|无|
 **属性**
