**[返回](Attribute.html "返回")**
# WxMenuGroup.class #

**介绍**

菜单组对象


**继承**

object.class 

**构造函数** 

 **属性**

| 类型 | 名称 |  说明 | 
| ---- | ---- |-----|
|string|Name|菜单组名称|
|WxMenuGroup|SubsetMenuGroup|子集菜单组对象|