**[返回](Attribute.html "返回")**
# MenuAttribute.class #

**介绍**

标识接口为菜单


**继承**

Attribute.class 

**构造函数** 

| 名称| 说明 |
| ---- | ---- | 
|MenuAttribute(string GroupName, int menuSite)|GroupName 属性为标识菜单分组位置标识（格式:“菜单组名称”->“子菜单组名称”,该格式可无限延长）；MenuSite为用户自定义菜单位置属性|
|MenuAttribute(int menuSite)
 **属性**

| 类型 | 名称 |  说明 | 
| ---- | ---- |-----|
|string|GroupName|菜单组名称字符串格式为“组名称->组名称->...”|
|int|IMenuSite|用户自定义菜单位置标识|
|[WxMenuGroup](WxMenuGroup.html)|MenuGroup|菜单组对象 |