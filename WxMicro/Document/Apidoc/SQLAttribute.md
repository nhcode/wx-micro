**[返回](Attribute.html "返回")**
# SQLAttribute.class #

**介绍**

属性解释

标识某属性为数据表字段

**继承**

Attribute.class 

**构造函数** 

| 名称| 说明 |
| ---- | ---- | 
|SQLAttribute(string SQLAttributeName)|SQLAttributeName数据表字段中的类型|

 **属性**

|类型|名称|说明|
|----|----|----|
|string|SQLAttributeName|数据表字段中的类型|
