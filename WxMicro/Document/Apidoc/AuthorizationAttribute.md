**[返回](Attribute.html "返回")**
# AuthorizationAttribute.class #

**介绍**

用户授权（需要用户登录并获得访问允许的相应权限才能访问）


**继承**

Attribute.class 

**构造函数** 

| 名称| 说明 |
| ---- | ---- | 
|无|
 **属性**
