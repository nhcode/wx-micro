**[返回](Attribute.html "返回")**
# ApiAttribute.class #

**介绍**

标识接口是API


**继承**

Attribute.class 

**构造函数** 

| 名称| 说明 |
| ---- | ---- | 
|ApiAttribute（string Name）|
|ApiAttribute（string Name,string [] subchain）|
 **属性**

| 类型 | 名称 |  说明 | 
| ---- | ---- |-----|
|string|Name|接口api的功能介绍名称|
|string[]|subchain|调用需要用到的接口地址数组|