**[返回](Attribute.html "返回")**
# PortServiceAttribute.class #

**介绍**

接口服务特性对象


**继承**

Attribute.class 

**构造函数** 

| 名称| 说明 |
| ---- | ---- | 
|PortServiceAttribute(Type type) | Controllrs对象的特性属性 |

 **属性**

| 类型 | 名称 |  说明 | 
| ---- | ---- |-----|
|HashSet<string>|FullNames|对象名称哈希存储列表|
|List<WxPort>|Ports|接口存储列表|

**方法**

| 返回类型 | 方法名称 |方法说明|
|----|----| ------------------------------- |
|string|GetControllerName（string ControllerName）|获取ControllerName名称|
|string|GetTypename(string Name)|获取属性类型|
|string|GetPresentation(ParameterInfo Parameter)|获取属性介绍|
|string|GetPresentation(PropertyInfo Parameter)|获取属性介绍|




