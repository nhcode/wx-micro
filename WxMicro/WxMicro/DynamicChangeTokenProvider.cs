﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace WxMicro
{
    /// <summary>
    /// 构建IActionDescriptorChangeProvider接口完成对接口模块加载的任务反射功能,以及通知mvc框架新模块加入
    /// </summary>
    public class DynamicChangeTokenProvider : IActionDescriptorChangeProvider
    {
        private CancellationTokenSource _source;
        private CancellationChangeToken _token;
        private static List<Assembly> Lists = new();
        /// <summary>
        /// 构造方法实现对象实例化，以及数据初始化
        /// </summary>
        /// <param name="manager">ApplicationPartManager 缓存组件</param>
        public DynamicChangeTokenProvider([FromServices] ApplicationPartManager manager)
        {
            _source = new CancellationTokenSource();
            _token = new CancellationChangeToken(_source.Token);
            foreach (var its in InitializeAssembly())
            {
                manager.ApplicationParts.Add(new AssemblyPart(its));
               NotifyChanges();
            }
        }
        /// <summary>
        /// CancellationChangeToken 调用
        /// </summary>
        /// <returns></returns>
        public IChangeToken GetChangeToken() => _token;

        /// <summary>
        /// 服务注册通知
        /// </summary>
        public void NotifyChanges()
        {
            var old = Interlocked.Exchange(ref _source, new CancellationTokenSource());
            _token = new CancellationChangeToken(_source.Token);
            old.Cancel();
        }


        /// <summary>
        /// 子服务加载
        /// </summary>
        public  HashSet<Assembly> InitializeAssembly()
        {
            string Folder = ".\\Submodular\\";
            if (!System.IO.Directory.Exists(Folder))
                System.IO.Directory.CreateDirectory(Folder);

            var Paths = GetFilePath(Folder);
            HashSet<Assembly> HasAss = new HashSet<Assembly>();
            foreach (var path in Paths)
            {
                Assembly assembly = Assembly.LoadFile(path);
                Lists.Add(assembly);
                foreach (var type in assembly.GetTypes())
                {
                    if (type.FullName.Contains("Controller"))
                    {
                        type.GetCustomAttributes(true);
                        HasAss.Add(assembly);
                    }
                }
            }
            return HasAss;
        }

        /// <summary> 
        /// 递归查找子模块文件夹下的子模块dll
        /// </summary>
        /// <param name="Exists">路径</param>
        /// <returns></returns>
        public  List<string> GetFilePath(string Exists)
        {
            List<string> Paths = new List<string>();
            DirectoryInfo folder = new DirectoryInfo(Exists);

            foreach (DirectoryInfo file in folder.GetDirectories())
            {
                if (System.IO.Directory.Exists(file.FullName))
                {
                    Paths.AddRange(GetFilePath(file.FullName));
                }
            }
            foreach (FileInfo file in folder.GetFiles("*.dll"))
            {
                Paths.Add(file.FullName);
            }
            return Paths;
        }
    }

}
