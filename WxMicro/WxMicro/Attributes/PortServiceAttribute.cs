﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WxUtile.Models.Ports;
using WxUtile.Strings.Encrypts;

namespace WxUtile.Attributes
{
    /// <summary>
    /// 接口文档构建特性对象
    /// </summary>
    public class PortServiceAttribute : Attribute
    {
        private static HashSet<string> FullNames = new();
        /// <summary>
        /// 接口文档存储对象
        /// </summary>
        public static List<WxPort> Ports = new();

        /// <summary>
        /// 接口文档构造函数
        /// 作用是根据反射对象获取到对应接口、以及请求方式、等接口参数的资料特性
        /// </summary>
        /// <param name="type">需要获取参数的对象类型</param>
        public PortServiceAttribute(Type type)
        {
            
            if (FullNames.Add(type.FullName))
            {
                ///检索出所有公开方法
                List<MethodInfo> Memberinfos = type.GetMethods().Where(o => o.DeclaringType.FullName == type.FullName && o.Name != ".ctor").ToList();

                ///判断该服务接口是需要进行授权或者获取用户资料
                bool IsAuthorization= type.GetCustomAttributes().Where(o => o.GetType().FullName == typeof(AuthorizationAttribute).FullName || o.GetType().FullName == typeof(UserDataAttribute).FullName).Count() > 0;
             
                ///判断该服务接口是否需要进行资料签名处理
                var IsSign = type.GetCustomAttributes().Where(o => o.GetType().FullName == typeof(SignDataAttribute).FullName).Count() > 0;

                ///获取路由列表
                var Route = type.GetCustomAttributes().Where(o => o.GetType().FullName == typeof(RouteAttribute).FullName).ToList();


                ///循环取出接口资料
                foreach (MethodInfo Memberinfo in Memberinfos)
                {
                    var Path = $"/{GetControllerName(type.Name)}/{Memberinfo.Name}";

                    var IsUrlToken = false;


                    if (Route.Count() > 0)
                    {
                        Path ="/"+((RouteAttribute)Route.First()).Template.ToLower().Replace("[controller]", GetControllerName(type.Name)).Replace("[action]", Memberinfo.Name);
                        Regex r = new ($"{{(.*?)}}");
                        MatchCollection mc = r.Matches(Path);
                        if (mc.Count() > 0)
                        {
                            IsUrlToken = true;
                            Path =Path.Replace("/"+mc[0].Value,"");
                        }
                    }
                    Path = Path.ToLower();

                    ///获取接口标注对象
                    var CustomAttributes = Memberinfo.GetCustomAttributes();

                    var Remark = CustomAttributes.Where(o => o.GetType().FullName == typeof(RemarkAttribute).FullName).ToList();

                    ///获取请求方法集合
                    var Methods = CustomAttributes.Where(o => o.GetType().BaseType.FullName == typeof(HttpMethodAttribute).FullName).ToList();

                    ///获取接口类型
                    Attribute CustomAttribute=null;

                    try {
                        ///获取接口类型
                         CustomAttribute = CustomAttributes.Where(o => o.GetType().FullName == typeof(ApiAttribute).FullName || o.GetType().FullName == typeof(PageAttribute).FullName).First();
                    }
                    catch
                    {
                        throw new Exception($"接口“{Path}”标注不明确或者未标注,具体标注方式请参照框架文档");
                    }

                    ///获取菜单标识
                    var MenuGroup = CustomAttributes.Where(o => o.GetType().FullName == typeof(MenuAttribute).FullName).ToList();

                    var IsApi = CustomAttribute.GetType().FullName == typeof(ApiAttribute).FullName;

                    WxPort PathPort = new()
                    {
                        Path = Path,
                        IsUrlToken = IsUrlToken,
                        ModuleName = type.FullName,
                        Name = ((ApiAttribute)CustomAttribute).Name,
                        PathType = IsApi ? "API" : "PAGE",
                        SubchainIDs = ((ApiAttribute)CustomAttribute).Subchain.Select(o => o.ToLower().GetMD5()).ToList(),
                        Remark = Remark.Count() > 0 ? ((RemarkAttribute)Remark.First()).Remark : "",
                    };

                    #region 请求方法
                    foreach (var Met in Methods)
                    {
                        if (PathPort.RequesMethod == null || PathPort.RequesMethod == "")
                        {
                            PathPort.RequesMethod = ((HttpMethodAttribute)Met).HttpMethods.First();
                        }
                        else
                        {
                            PathPort.RequesMethod = $"{ PathPort.RequesMethod }|{ ((HttpMethodAttribute)Met).HttpMethods.First()}";
                        }
                    }
                    #endregion

                    #region 登录规则识别
                    if (IsAuthorization)
                    {
                        PathPort.IsAuthorization = IsAuthorization;
                    }
                    else
                    {
                        PathPort.IsAuthorization = CustomAttributes.Where(o => o.GetType().FullName == typeof(AuthorizationAttribute).FullName).Count() > 0;
                    }
                    if (IsSign)
                    {
                        PathPort.IsSign = IsSign;
                        PathPort.IsAuthorization = IsSign;
                    }
                    else
                    {
                        IsSign = CustomAttributes.Where(o => o.GetType().FullName == typeof(SignDataAttribute).FullName).Count() > 0;
                        if (IsSign)
                        {
                            PathPort.IsSign = IsSign;
                            PathPort.IsAuthorization = IsSign;
                        }
                        else
                        {
                            PathPort.IsSign = false;
                        }
                    }
                    if (PathPort.IsAuthorization)
                    {
                        PathPort.IsUserValidation = PathPort.IsAuthorization;
                    }

                    #endregion

                    #region 菜单识别

                    PathPort.IsMenu = MenuGroup.Count() > 0;
                    if (PathPort.IsMenu)
                    {
                        MenuAttribute Group = (MenuAttribute)MenuGroup.First();
                        PathPort.MenuLocation = Group.IMenuSite;
                        PathPort.MenuGroupName = Group.GroupName;
                        PathPort.MenuGroup = Group.MenuGroup;
                    }

                    #endregion

                    #region 请求参数识别
                    List<ParameterInfo> Parameters = Memberinfo.GetParameters().ToList();
                    List<WxParameter> AfferentParameter = new ();
                    foreach (var Parameter in Parameters)
                    {
                        var ParamettypeAutts = Parameter.ParameterType.GetCustomAttributes().Where(o => o.GetType().FullName == typeof(ExplainAttribute).FullName).ToList();
                        if (ParamettypeAutts.Count > 0)
                        {
                            List<PropertyInfo> PropertyInfos = Parameter.ParameterType.GetProperties().ToList();
                            foreach (var PropertyInfo in PropertyInfos)
                            {
                                AfferentParameter.Add(new WxParameter
                                {
                                    ParameterName = PropertyInfo.Name,
                                    ParameterType = GetTypeName(PropertyInfo.PropertyType.FullName),
                                    ParameterSpecification = GetPresentation(PropertyInfo),
                                    IsNotNull = PropertyInfo.GetCustomAttributes().Where(o => o.GetType().FullName == typeof(NotNullAttribute).FullName).Count() > 0
                                }) ;
                            }
                        }
                        else
                        {
                            AfferentParameter.Add(new WxParameter
                            {
                                ParameterName = Parameter.Name,
                                ParameterType = GetTypeName(Parameter.ParameterType.FullName),
                                ParameterSpecification = GetPresentation(Parameter),
                                IsNotNull = Parameter.GetCustomAttributes().Where(o => o.GetType().FullName == typeof(NotNullAttribute).FullName).Count() > 0
                            });
                        }

                    }


                    if (PathPort.IsSign)
                    {
                        AfferentParameter.Add(new WxParameter
                        {
                            ParameterName = "Sign",
                            ParameterType = GetTypeName(typeof(string).FullName),
                            ParameterSpecification = "内容数据签名",
                            IsNotNull = true,
                            Remark = System.Web.HttpUtility.UrlEncode(@"加密与签名规则如下:
                                        1.生产平台所需的公私钥，私钥自行保管，并将公钥私钥提交平台
                                        2.通过申请，提交平台所需的的相关文件。
                                        3.平台返回请求接口所需要的资料有APPID、数据签名公钥等。
                                        4.构建请求对象。将请求字段按照ASCII码增序（字母升序排序）
                                        ①筛选
                                        获取所有请求参数，不包括字节型参数，如文件、字节流，剔除sign字段。
                                        ②排序
                                        将筛选的参数按照第一个字符的键值ASCII码递增排序（字母升序排序），如果遇到相同字符则按照第二个字符的键值ASCII码递增排序，以此类推。
                                        ③拼接
                                        将排序后的参数与其对应值，组合成“参数=参数值”的格式，并且把这些参数用&字符连接来，此时生成的字符串为待签名字符串。
                                        将请求连接地址（请求域名后接口地址）与排序后的待签名字符串（通过“? ”）拼接。（示例：“/api/base/test?appid=123&sign=123”）
                                        ④调用签名函数
                                        现将拼接后的参数，按照编码类型处理为byte数组，使用各自语言对应的RSA签名函数利用商户私钥对待签名字符串进行签名，并进行Base64编码。
                                        5.请求接口。
                                           Post请求：
                                           ①SignType、 Sign作为Query请求参数，其余数据作为Body请求参数。
                                           ②提交请求获取数据
                                           Get请求：
                                           ①将所有请求参数包含SignType、Sign作为Query请求参数
                                           ②提交请求获取数据")
                        });

                        AfferentParameter.Add(new WxParameter
                        {
                            ParameterName = "SignType",
                            ParameterType = GetTypeName(typeof(string).FullName),
                            ParameterSpecification = "签名算法类型",
                            IsNotNull = true,
                            Remark = "签名类型算法只支持：RSA2"

                        });
                        AfferentParameter.Add(new WxParameter
                        {
                            ParameterName = "Timestamp",
                            ParameterType = GetTypeName(typeof(string).FullName),
                            ParameterSpecification = "当前时间毫秒值",
                            IsNotNull = true,
                            Remark = "当前时间的毫秒值"

                        });
                        PathPort.Remark = System.Web.HttpUtility.UrlEncode(@"加密与签名规则如下:
                                        1.生产平台所需的公私钥，私钥自行保管，并将公钥私钥提交平台
                                        2.通过申请，提交平台所需的的相关文件。
                                        3.平台返回请求接口所需要的资料有APPID、数据签名公钥等。
                                        4.构建请求对象。将请求字段按照ASCII码增序（字母升序排序）
                                        ①筛选
                                        获取所有请求参数，不包括字节型参数，如文件、字节流，剔除sign字段。
                                        ②排序
                                        将筛选的参数按照第一个字符的键值ASCII码递增排序（字母升序排序），如果遇到相同字符则按照第二个字符的键值ASCII码递增排序，以此类推。
                                        ③拼接
                                        将排序后的参数与其对应值，组合成“参数=参数值”的格式，并且把这些参数用&字符连接来，此时生成的字符串为待签名字符串。
                                        将请求连接地址（请求域名后接口地址）与排序后的待签名字符串（通过“? ”）拼接。（示例：“/api/base/test?appid=123&sign=123”）
                                        ④调用签名函数
                                        现将拼接后的参数，按照编码类型处理为byte数组，使用各自语言对应的RSA签名函数利用商户私钥对待签名字符串进行签名，并进行Base64编码。
                                        5.请求接口。
                                           Post请求：
                                           ①SignType、 Sign作为Query请求参数，其余数据作为Body请求参数。
                                           ②提交请求获取数据
                                           Get请求：
                                           ①将所有请求参数包含SignType、Sign作为Query请求参数
                                           ②提交请求获取数据");
                    }

                    PathPort.Parameters = AfferentParameter;
                    #endregion

                    Ports.Add(PathPort);
                }
            }
            
        }

        /// <summary>
        /// 获取Controller名称
        /// </summary>
        /// <param name="ControllerName">对应ControllerName的类型名称</param>
        /// <returns></returns>
        public string GetControllerName(string ControllerName)
        {
            return ControllerName.Replace("Controller", "");
        }
     
        /// <summary>
        /// 获取属性类型
        /// </summary>
        /// <param name="Name">属性名称</param>
        /// <returns></returns>
        public string GetTypeName(string Name)
        {
            var index = Name.LastIndexOf(".");

            return Name.Substring(index + 1, Name.Length - 1 - index);
        }
      
        /// <summary>
        /// 获取属性介绍
        /// </summary>
        /// <param name="Parameter">反射获取的属性对象</param>
        /// <returns></returns>
        public string GetPresentation(ParameterInfo Parameter)
        {
            try
            {
                return ((ExplainAttribute)Parameter.GetCustomAttributes().Where(o => o.GetType().FullName == typeof(ExplainAttribute).FullName).First()).Value;
            }
            catch
            {
                return "未备注属性解释";
            }

        }
        /// <summary>
        /// 获取属性介绍
        /// </summary>
        /// <param name="Parameter">反射获取的属性对象</param>
        /// <returns></returns>
        public string GetPresentation(PropertyInfo Parameter)
        {
            try
            {
                return ((ExplainAttribute)Parameter.GetCustomAttributes().Where(o => o.GetType().FullName == typeof(ExplainAttribute).FullName).First()).Value;
            }
            catch
            {
                return "未备注属性解释";
            }

        }
    }
    /// <summary>
    /// 备注特性，提供程序员备注对应接口的文档资料
    /// </summary>
    public class RemarkAttribute : Attribute { 
    
        /// <summary>
        /// 备注资料属性内容
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 构建备注特性的属性构造函数
        /// </summary>
        /// <param name="Remark">备注说明内容</param>
        public RemarkAttribute(string Remark)
        {
            this.Remark = Remark;
        }

    }
    /// <summary>
    /// 用户授权（需要用户登录并获得访问允许的相应权限才能访问）
    /// </summary>
    public class AuthorizationAttribute : Attribute{}

    /// <summary>
    /// 用户资料（需要使用用户资料，但不需要用户获得授权就可以访问）
    /// </summary>
    public class UserDataAttribute : Attribute{}

    /// <summary>
    /// 用户请求接口时需要对资料进行签名处理）
    /// </summary>
    public class SignDataAttribute : Attribute{}

    /// <summary>
    /// 标识接口是API
    /// </summary>
    public class ApiAttribute : Attribute{
    
        /// <summary>
        /// 接口名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 子接口列表
        /// </summary>
        public string[] Subchain { get; set; } = new string[0];

        /// <summary>
        /// 特性构造函数
        /// </summary>
        /// <param name="Name">接口名称</param>
        public ApiAttribute(string Name)
        {
            this.Name = Name;
        }
        /// <summary>
        /// 特性构造函数
        /// </summary>
        /// <param name="Name">接口名称</param>
        /// <param name="Subchain">子接口链接地址</param>
        public ApiAttribute(string Name, string[] Subchain)
        {
            this.Name = Name;
            this.Subchain = Subchain;
        }

    }
    /// <summary>
    /// 标识接口是Page
    /// </summary>
    public class PageAttribute : ApiAttribute
    {
        /// <summary>
        /// 特性构造函数
        /// </summary>
        /// <param name="Name">接口名称</param>
        public PageAttribute(string Name) : base(Name)
        {
            this.Name = Name;
        }
        /// <summary>
        /// 特性构造函数
        /// </summary>
        /// <param name="Name">接口名称</param>
        /// <param name="Subchain">子接口链接地址</param>
        public PageAttribute(string Name, string[] Subchain):base(Name,Subchain)
        {
            this.Name = Name;
            this.Subchain = Subchain;
        }
    }
    /// <summary>
    /// 标识接口为菜单
    /// </summary>
    public class MenuAttribute : Attribute
    {
        /// <summary>
        /// 菜单组名称
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// 菜单组对象
        /// </summary>
        public WxMenuGroup MenuGroup { get; set; }

        /// <summary>
        /// 菜单位置标识（用户自定义标识位置所在）
        /// </summary>
        public int IMenuSite { get; set; }
        /// <summary>
        /// 构造菜单标识对象
        /// </summary>
        /// <param name="GroupName">菜单组名称</param>
        /// <param name="menuSite">菜单位置标识</param>
        public MenuAttribute(string GroupName, int menuSite)
        {
            this.GroupName = GroupName;

            string[] item = GroupName.Split("->");

            WxMenuGroup wxMenu = null;
            for (var i = item.Length - 1; i >= 0; i--)
            {
                var Name = item[i];
                wxMenu = new()
                {
                    Name = Name,
                    SubsetMenuGroup = wxMenu
                };
            }
            MenuGroup = wxMenu;
            IMenuSite = menuSite;
        }
        /// <summary>
        /// 构造菜单标识对象
        /// </summary>
        /// <param name="menuSite">菜单位置标识</param>
        public MenuAttribute(int menuSite)
        {
            GroupName = "";
            MenuGroup = new();
            IMenuSite = menuSite;
        }
    }
}
