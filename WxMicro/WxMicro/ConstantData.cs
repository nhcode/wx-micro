﻿using WxUtile.Models;

namespace WxUtile
{
    /// <summary>
    /// 数据存储对象
    /// </summary>
    public class ConstantData
    {
        /// <summary>
        /// 用户配置的证书文件存储
        /// </summary>
         public static WxTheISsuedBy ISsuedBy { get; set; }
        /// <summary>
        /// Redis存储
        /// </summary>
        public static CSRedis.CSRedisClient CSRedisClient { get; set; }

        /// <summary>
        /// 证书有效时间
        /// </summary>
        public static long CertificateExpirationTime { get; set; }

        /// <summary>
        /// 消息签名有效时间
        /// </summary>
        public static long MsgMinutes { get; set; }
    }
}
