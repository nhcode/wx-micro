﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using WxUtile.DataBase;

namespace WxUtile.Configs
{
    /// <summary>
    /// 获取配置项工具
    /// </summary>
    public class WxUtileConfig
    {
        private static IConfiguration _configuration;
        private WxUtileConfig(string fileName)
        {
            //在当前目录或者根目录中寻找Config.json文件

            var directory = AppContext.BaseDirectory;
            directory = directory.Replace("\\", "/");

            var filePath = $"{directory}/{fileName}";
            if (!File.Exists(filePath))
            {
                var length = directory.IndexOf("/bin");
                filePath = $"{directory.Substring(0, length)}/{fileName}";
            }

            var builder = new ConfigurationBuilder()
                .AddJsonFile(filePath, false, true);

            _configuration = builder.Build();
        }

        public static WxUtileConfig GetConfig(string PateConfig)
        {
            return new WxUtileConfig(PateConfig);
        }
        
        /// <summary>
        /// 读取配置
        /// </summary>
        /// <param name="key">配置项键</param>
        /// <returns></returns>
        public  string GetValue(string key)
        {
            return _configuration.GetSection(key).Value;
        }

        /// <summary>
        /// 获取对象
        /// </summary>
        /// <typeparam name="T">需要获取的结构对象</typeparam>
        /// <param name="Key">配置项键</param>
        /// <returns></returns>
        public  T GetOBject<T>(string Key)
        {
            return _configuration.GetSection(Key).Get<T>();
        }

        /// <summary>
        /// 获取数据连接对象
        /// </summary>
        /// <param name="Key">配置项键</param>
        /// <returns></returns>
        public  WxDbData GetDbData(string Key)
        {
            return GetOBject<WxDbData>(Key);
        }
    }
}
