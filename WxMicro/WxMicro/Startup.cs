﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Rewrite;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using WxUtile.Authorizs;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using WxMicro;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace WxUtile
{
    /// <summary>
    /// startup类
    /// </summary>
    public class Startup
    {
        private Interfaces.IStartup IStart { get; set; }
        /// <summary>
        /// 初始化方法
        /// </summary>
        /// <param name="configuration">IConfiguration 对象</param>
        /// <param name="IStart">IStartup接口对象</param>
        public void initialize(IConfiguration configuration, Interfaces.IStartup IStart) 
        {
            Configuration = configuration;
            this.IStart = IStart;
            ConstantData.ISsuedBy = IStart.GetTheISsuedBy();
            ConstantData.CSRedisClient = new CSRedis.CSRedisClient(IStart.GetRedisConnecting());
            ConstantData.CertificateExpirationTime = IStart.GetAuthorizService().CertificateExpirationTime();
            ConstantData.MsgMinutes = IStart.GetAuthorizService().MsgMinutes();
            IStart.Initialize();
        }
        
        /// <summary>
        /// 配置常量
        /// </summary>
        public IConfiguration Configuration { get; set; }

        /// <summary>
        /// 服务配置
        /// </summary>
        /// <param name="services">IServiceCollection 对象</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //配置允许跨域请求
            services.AddCors(options =>
            {
                options.AddPolicy("allow_all", builder =>
                {
                    builder.AllowAnyOrigin() //允许任何来源的主机访问
                    .AllowAnyMethod()
                    .AllowAnyHeader();
                });
            });

            //取消上传限制
            services.Configure<FormOptions>(x => x.MultipartBodyLengthLimit = 3517122560);


            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.AreaViewLocationFormats.Clear();
                options.AreaViewLocationFormats.Add("/Views/Shared/{0}.cshtml");
                options.AreaViewLocationFormats.Add("/Views/{1}/{0}.cshtml");
            });


            services.AddControllersWithViews();
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                //忽略循环引用
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                //不使用驼峰样式的key
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();

            });
            services.AddControllers().AddRazorPagesOptions(o =>
            {
                o.RootDirectory = "/Pages/{1}";

            });
          
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton<DynamicChangeTokenProvider>()
                    .AddSingleton<IActionDescriptorChangeProvider>(provider => provider.GetRequiredService<DynamicChangeTokenProvider>());
        }

        /// <summary>
        /// 路由配置
        /// </summary>
        /// <param name="app">IApplicationBuilder</param>
        /// <param name="env">IHostingEnvironment</param>
        [Obsolete]
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            //重写路由
            var rewrite = new RewriteOptions()
            .AddRewrite("(.*)/_blazor/negotiate", "_blazor/negotiate", skipRemainingRules: true)
            .AddRedirect("(.*)/_blazor/negotiate", "_blazor/negotiate")
            .AddRewrite("(.*)/_blazor", "_blazor", skipRemainingRules: true)
            .AddRedirect("(.*)/_blazor", "_blazor");

            app.UseRewriter(rewrite);

            app.UseStaticFiles(); //允许资源

            app.UseRouting();

            app.UseHttpsRedirection();

            app.UseDefaultFiles();
            app.UseCors("allow_all");//跨域中间件
            app.UseCookiePolicy(); //允许cookie

            app.UseAuthentication();

            //注册webSocket
            app.UseWebSockets();

            app.UseMiddleware<WxAuthorizService>(IStart.GetAuthorizService());// 权限验证中间件

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
                endpoints.MapBlazorHub();
            });
        }
    }
}
