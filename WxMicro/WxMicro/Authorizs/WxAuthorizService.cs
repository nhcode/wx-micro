﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WxUtile.Attributes;
using WxUtile.Interfaces;
using WxUtile.Models;
using WxUtile.Models.Ports;
using WxUtile.Data;
using WxUtile.Requests;
using WxUtile.Strings;
using WxUtile.Strings.Encrypts;
using WxUtile.Date;
using System.Security.Cryptography;
using System.Text;

namespace WxUtile.Authorizs
{
    /// <summary>
    /// 用户权限鉴别对象中间件
    /// </summary>
    public class WxAuthorizService
    {
        private RequestDelegate Next { get; }
        /// <summary>
        /// 用户服务权限接口对象
        /// </summary>
        private IAuthorizService IAutService { get; }


        /// <summary>
        /// 构造函数，构建用户鉴权中间件对象
        /// </summary>
        /// <param name="Next">请求委托</param>
        /// <param name="IAutService">用户服务权限接口对象</param>
        public WxAuthorizService(RequestDelegate Next, IAuthorizService IAutService)
        {
            this.Next = Next;
            this.IAutService = IAutService;
        }
        /// <summary>
        /// 用户与委托通道中间对象
        /// </summary>
        /// <param name="httpContext">HttpContext对象</param>
        /// <returns></returns>
        public async Task InvokeAsync(HttpContext httpContext)
        {
            //获取用户请求地址
            string Path = httpContext.Request.Path.ToString().ToLower();

            //获取配置是否允许的 文件请求白名单正则
            
            var ResourceTypes = IAutService.GetPublicResourceTypes().Replace(".", "\\.");
            //判断白名单资料是否为null
            if (ResourceTypes != null)
            {
                //构建正则校验
                Regex r = new  ($"(.*)wwwroot/(.*)(?={ResourceTypes})");
                MatchCollection mc = r.Matches(Path);
                
                //判断校验是否成功
                if (mc.Count > 0)
                {
                    await Next.Invoke(httpContext);
                    return;
                }
            }
            //允许Blazor组件支持请求
            if (Path.Contains("/_blazor/negotiate") || Path.Contains("/_blazor") || Path.Contains("/_framework/blazor.server.js"))
            {
                await Next.Invoke(httpContext);
                return;
            }

            var Token = "";

            WxPort Port = null;

            #region 连接检测

            var wxPorts1 = PortServiceAttribute.Ports.Where(o => o.ID == Path.GetMD5().ToUpper()).ToList();

            if (wxPorts1.Count() > 0)
            {
                Port = wxPorts1.Single();
            }
            else
            {
                var index = Path.LastIndexOf("/");
                 Token = Path.Substring(index+1, Path.Length - index-1);
                Path = Path.Substring(0, index);
                var wxPorts = PortServiceAttribute.Ports.Where(o => o.ID == Path.GetMD5().ToUpper()).ToList();
                if (wxPorts.Count() > 0)
                {
                    Port = wxPorts.Single();
                }
            }
            #endregion
            
            //判断链接请求是否存在
            if (Port != null)
            {
                WxCertificate<WxUser> wxCertificate = null;
                string Msg = "";
                var auth = false;
                //判断接口是否需要获得用户资料
                if (Port.IsUserValidation)
                {
                    //获取用户资料证书
                    auth= Port.IsUrlToken?Token.GetAuthorization(httpContext.GetIP(),ref wxCertificate, ref Msg)?true: httpContext.GetAuthorization(ref wxCertificate, ref Msg) : httpContext.GetAuthorization(ref wxCertificate, ref Msg)?true: Token.GetAuthorization(httpContext.GetIP(), ref wxCertificate, ref Msg);
                  
                    //判断是否获取成功
                    if (auth)
                    {
                        //判断该接口是否需要权限验证
                        if (Port.IsAuthorization)
                        {
                            //验证用户是否有权访问该项接口
                            if (wxCertificate.AuthPortIDs.Where(o => o.ToUpper() == Port.ID).Count() > 0) {

                                if (Port.IsSign)
                                {
                                    #region 签名校验

                                    var from = httpContext.Request.Form.ToList();
                                    from.AddRange(httpContext.Request.Query.ToList());
                                    var ITimestamp = from.Where(o => o.Key == "Timestamp");
                                    if (ITimestamp.Count() <= 0)
                                    {
                                        await httpContext.SendMsg(200, WxMsg.Set(wxCertificate.PrivateKey, false, "请上送Timestamp参数！", WxUtile.Enums.WxEMsg.FAILED).Value);
                                        return;
                                    }

                                    var ISignType = from.Where(o => o.Key == "SignType");
                                    if (ISignType.Count() <= 0)
                                    {
                                        await httpContext.SendMsg(200, WxMsg.Set(wxCertificate.PrivateKey, false, "请上送SignType参数！", WxUtile.Enums.WxEMsg.FAILED).Value);
                                        return;
                                    }
                                    var SignType = ISignType.Single().Value;

                                    var ISign = from.Where(o => o.Key == "Sign");

                                    if (ISign.Count() <= 0)
                                    {
                                        await httpContext.SendMsg(200, WxMsg.Set(wxCertificate.PrivateKey, false, "请上送Sign参数！", WxUtile.Enums.WxEMsg.FAILED).Value);
                                        return;
                                    }

                                    var Sign = ISign.Single().Value.ToString();

                                    var SignData = from.Where(o => o.Key != "Sign").ToList();
                                    Dictionary<string, string> keyValues = new ();
                                    foreach (var idata in SignData)
                                    {
                                        keyValues.Add(idata.Key, idata.Value);
                                    }
                                    string WSign = $"{Path}?{keyValues.GetParamStr()}";
                                    try
                                    {
                                        var Timestamp = long.Parse(ITimestamp.First().Value);
                                        long NowTimestamp = DateTime.Now.GetTimestamp();
                                        double difference = NowTimestamp - Timestamp;
                                        double Timeout = difference.GetSecondToMinutes();
                                        if (0 <= Timeout && Timeout <= ConstantData.MsgMinutes)
                                        {
                                            if (SignType == "RSA2")
                                            {
                                                if (WSign.verifySHA256(Sign, wxCertificate.UserPublicKey))
                                                {
                                                    await Next.Invoke(httpContext); //访问网站
                                                    return;
                                                }
                                                await httpContext.SendMsg(200, WxMsg.Set(wxCertificate.PrivateKey, false, "签名验证失败！", WxUtile.Enums.WxEMsg.FAILED).Value);
                                                return;
                                            }
                                            else
                                            {
                                                await httpContext.SendMsg(200, WxMsg.Set(wxCertificate.PrivateKey, false, "签名类型不正确！只支持：RSA2！", WxUtile.Enums.WxEMsg.FAILED).Value);
                                                return;
                                            }
                                                   
                                        }
                                        else
                                        {

                                            await httpContext.SendMsg(200, WxMsg.Set(wxCertificate.PrivateKey,false, $"Timestamp消息验证失败，数据有效时间为{ConstantData.MsgMinutes}分钟内有效", WxUtile.Enums.WxEMsg.FAILED).Value);
                                            return;
                                        }
                                    }
                                    catch
                                    {
                                        await httpContext.SendMsg(200, WxMsg.Set(wxCertificate.PrivateKey, false, "签名验证失败", WxUtile.Enums.WxEMsg.FAILED).Value);
                                        return;
                                    }

                                    #endregion
                                }
                                else
                                {
                                    await Next.Invoke(httpContext);
                                    return;
                                }
                            }
                            else
                            {
                                await httpContext.SendMsg(200, WxMsg.Set(false, "无权访问！", WxUtile.Enums.WxEMsg.FAILED).Value);
                                return;
                            }
                        }
                        else
                        {
                            await Next.Invoke(httpContext);
                            return; 
                        }
                    }
                    else
                    {
                        await httpContext.SendMsg(200, WxMsg.Set(false, Msg, WxUtile.Enums.WxEMsg.FAILED).Value);
                        return;
                    }
                }
                else
                {
                    await Next.Invoke(httpContext);
                    return;
                }
            }
            else
            {
              await httpContext.SendMsg(404, WxMsg.Set(false, "访问地址不存在", WxUtile.Enums.WxEMsg.FAILED).Value);
                return;
            }
        }
    }
}
