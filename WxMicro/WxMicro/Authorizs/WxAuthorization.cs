﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WxUtile.Models;
using WxUtile.Requests;
using WxUtile.Strings;
using WxUtile.Strings.Encrypts;

namespace WxUtile.Authorizs
{
    /// <summary>
    /// 用户权限证书对象
    /// </summary>
    public static class WxAuthorization
    {
     /// <summary>
     /// 获取用户证书对象Token
     /// </summary>
     /// <param name="httpContext">HttpContext对象</param>
     /// <param name="Ca">用证书存储对象</param>
     /// <param name="Msg">获取情况消息</param>
     /// <returns></returns>
        public static bool GetAuthorization(this HttpContext httpContext,ref WxCertificate<WxUser> Ca,ref string Msg)
        {
            var Authorization = httpContext.Request.Headers["Authorization"].ToString();
            //判断用户证书是否存在
            if (Authorization == null || Authorization == "")
            {
                Msg = "未检测到Token证书!";
                return false;
            }
            //获取用户证书编号
            var Aation = Authorization[7..];
            //得到用户证书ID
            var ID = Aation.Substring(Aation.Length - 32, 32);
           //获取证书路径
            string Path = $".\\Certificates\\{ID.ToUpper()}.ca";
            //判断用户叏路径是否存在
            if (File.Exists(Path))
            {
                //读取用户证书文件
                using BinaryReader reader = new(File.Open(Path, FileMode.Open));

                var Data = reader.ReadString().AesDecrypt(ConstantData.ISsuedBy.SecurityKey);
                //获取用户证书信息
                Ca = Data.FromJson<WxCertificate<WxUser>>();

                //判断用户证书信息是否存在
                if (Ca != null)
                {
                    //构建用户证书
                    var ca = new WxCa
                    {
                        ID = Ca.User.ID,
                        PublicKey = Ca.PublicKey
                    };
                    //校验用户证书是否正确
                    if (ca.AuthorizationBearer.ToUpper().GetMD5() == Authorization.ToUpper().GetMD5())
                    {
                        //校验用户证书时间是否有效
                        if (DateTime.Now < Ca.ExpiringDate)
                        {
                            //获取用户客户端IP地址
                            string IP = httpContext.GetIP();
                            //校验用户IP是否匹配
                            if (IP == Ca.LoginIP)
                            {
                                reader.Close();
                                //重新写入用户证书，更新用户证书有效时间，证书校验成功
                                using (BinaryWriter writer = new  (System.IO.File.Open(Path, FileMode.Create)))
                                {
                                    Ca.ExpiringDate = DateTime.Now.AddMinutes(ConstantData.CertificateExpirationTime);
                                    writer.Write(Ca.ToJson().AesEncrypt(ConstantData.ISsuedBy.SecurityKey));
                                }
                                Msg = "Token校验成功";
                                return true;
                            }
                            else
                            {
                                reader.Close();
                                //重新写入用户证书，更新用户证书有效时间，令证书失效
                                using (BinaryWriter writer = new  (System.IO.File.Open(Path, FileMode.Create)))
                                {
                                    Ca.ExpiringDate = DateTime.Now.AddDays(-9999);
                                    writer.Write(Ca.ToJson().AesEncrypt(ConstantData.ISsuedBy.SecurityKey));
                                }
                                Msg = "证书已失效！IP地址异常";
                                return false;
                            }
                        }
                        else
                        {
                            Msg = "证书已失效！";
                            return false;
                        }
                    }
                    else
                    {
                        Msg = "Token证书校验失败!";
                        return false;
                    }
                }
                else
                {
                    Msg = "证书资料不存在，证书资料无效！";
                    return false;
                }
            }
            else
            {
                Msg = "证书资料不存在，证书资料无效！";
                return false;
            }
        }
      
     
        /// <summary>
        /// 获取用户证书对象Token
        /// </summary>
        /// <param name="Aation">用户证书编号</param>
        /// <param name="IP">客户端IP地址</param>
        /// <param name="Ca">用户帧数存储对象</param>
        /// <param name="Msg">获取情况消息</param>
        /// <returns></returns>
        public static bool GetAuthorization(this string Aation, string IP,ref WxCertificate<WxUser> Ca,ref string Msg)
        {
            //判断证书编号是否有效
            if (Aation == "" || Aation.Length < 32)
            {
                Msg = "token证书无效";
                return false;
            }
            //得到用户ID
            var ID = Aation.Substring(Aation.Length - 32, 32);
           
            //获取用户证书文件
            string Path = $".\\Certificates\\{ID.ToUpper()}.ca";
            //判断用户证书是否存在
            if (File.Exists(Path))
            {
                //读取用户证书文件
                using (BinaryReader reader = new(File.Open(Path, FileMode.Open)))
                {
                    //获取用户证书数据，并解密
                    var Data = reader.ReadString().AesDecrypt(ConstantData.ISsuedBy.SecurityKey);
                    //得到用证书对象
                   Ca = Data.FromJson<WxCertificate<WxUser>>();
                    //判断用证书对象是否为空
                    if (Ca != null)
                    {
                        //构建用户证书对象
                        var ca = new WxCa
                        {
                            ID = Ca.User.ID,
                            PublicKey = Ca.PublicKey
                        };
                        //判断用户证书是否符合用户授权
                        if (ca.AuthorizationUrl.ToUpper().GetMD5() == Aation.ToUpper().GetMD5())
                        {
                            //判断证书有效时间
                            if (DateTime.Now < Ca.ExpiringDate)
                            {
                                //校验客户端IP地址
                                if (IP == Ca.LoginIP)
                                {
                                    reader.Close();
                                    //重新写入用用户证书对象
                                    using (BinaryWriter writer = new (System.IO.File.Open(Path, FileMode.Create)))
                                    {
                                        Ca.ExpiringDate = DateTime.Now.AddMinutes(ConstantData.CertificateExpirationTime);
                                        writer.Write(Ca.ToJson().AesEncrypt(ConstantData.ISsuedBy.SecurityKey));
                                    }

                                    Msg = "Token校验成功";
                                    return true;
                                }
                                else
                                {
                                    reader.Close();
                                    //重写写入用户证书对象，令用用户证书失效
                                    using (BinaryWriter writer = new (System.IO.File.Open(Path, FileMode.Create)))
                                    {
                                        Ca.ExpiringDate = DateTime.Now.AddDays(-9999);
                                        writer.Write(Ca.ToJson().AesEncrypt(ConstantData.ISsuedBy.SecurityKey));
                                    }
                                    Msg = "证书已失效！IP地址异常";
                                    return false;
                                }

                            }
                            else
                            {
                                Msg = "证书已失效！";
                                return false;
                            }
                        }
                        else
                        {
                            Msg = "Token证书校验失败!";
                            return false;
                        }
                    }
                    else
                    {
                        Msg = "证书资料不存在，证书资料无效！";
                        return false;
                    }
                }
            }
            else
            {
                Msg = "证书资料不存在，证书资料无效！";
                return false;
            }
        }
    }
}
