﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WxUtile.Strings.Encrypts;

namespace WxUtile.Authorizs
{
    public class WxCa
    {
        /// <summary>
        /// 证书编号
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 证书证书值（PublicKey）服务端公钥
        /// </summary>
        public string PublicKey { get; set; }

        /// <summary>
        /// 获取Authorization
        /// </summary>
        public string Authorization => $"{PublicKey}{ID.GetMD5().ToUpper()}";

        /// <summary>
        /// 获取 Bearer Authorization
        /// </summary>
        public string AuthorizationBearer=> $"Bearer {PublicKey}{ID.GetMD5().ToUpper()}";

        /// <summary>
        /// 获取URL Authorization
        /// </summary>
        public string AuthorizationUrl=> $"{PublicKey.GetMD5().ToUpper()}{ID.GetMD5().ToUpper()}";

    }
}
