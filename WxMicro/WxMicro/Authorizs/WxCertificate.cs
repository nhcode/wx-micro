﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WxUtile.Interfaces;
using WxUtile.Models;
using WxUtile.Strings.Encrypts;

namespace WxUtile.Authorizs
{
    /// <summary>
    /// 用户证书资料 
    /// </summary>
    public class WxCertificate<T>
    {
       
        /// <summary>
        /// 证书公钥
        /// </summary>
        public string PublicKey { get; set; }

        /// <summary>
        /// 证书私钥
        /// </summary>
        public string PrivateKey { get; set; }

        /// <summary>
        /// 用户提供的RSA265-2048的公钥串
        /// </summary>
        public  string UserPublicKey { get; set; }

        /// <summary>   
        /// 证书到期时间
        /// </summary>
        public DateTime ExpiringDate  { get; set; }
        
        /// <summary>
        /// 证书颁发日期
        /// </summary>
        public DateTime IssuanceDate { get; set; }
        
        /// <summary>
        /// 用户登录IP地址
        /// </summary>
        public string LoginIP { get; set; }

        /// <summary>
        /// 用户允许使用连接授权接口ID数组
        /// </summary>
        /// <returns></returns>
        public List<string> AuthPortIDs { get; set; }

        /// <summary>
        /// 证书使用者资料
        /// </summary>
        public T User { get; set; } 


    }
}
