﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using WxUtile.Log;
using WxUtile.Data;
using System.Text;

namespace WxUtile.DataBase
{
    /// <summary>
    /// Entity Framework 数据库连接对象
    /// </summary>
    public abstract class WxBaseContent : DbContext
    {
        private  static bool initialize = true;

        /// <summary>
        /// 获取连接配置方法
        /// </summary>
        /// <returns></returns>
        public abstract WxDbData GetDbData();

        /// <summary>
        /// 数据库连接字段配置
        /// </summary>
        protected  override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (initialize)
            {
                throw new Exception("数据库对象未进行初始化，在启动时或者在Steup对象中调用Initialize()");
            }

            WxDbData DbData = GetDbData();
            switch (DbData.SQL.Trim().ToLower())
            {
                case "sqlserver":
                    optionsBuilder.UseSqlServer(DbData.DB);
                    break;
                case "mysql":
                    optionsBuilder.UseMySQL(DbData.DB);
                    break;
                case "sqlite":
                    optionsBuilder.UseSqlite(DbData.DB);
                    break;
                case "oracle":
                    optionsBuilder.UseOracle(DbData.DB);
                    break;
            }
            optionsBuilder.LogTo(Message => Message.ToLogRecord("DbContentConfigUring")) //记录数据库日志
            //允许放回数据错误日志
            .EnableSensitiveDataLogging()
            ;

        }
       
            /// <summary>
            /// 初始化数据表格，该算法是通过用户反射方式以及C#特性属性获取对应参数实现 C#对象转SQL命令，并写入数据库完成数据表构建的数据函数
            /// </summary>
        public void Initialize()
        {
            try
            {
                //记录数据标识，标书数据库已被初始化
                initialize = false;
                //获取当前的数据库构建对象
                var type = this.GetType();
                //根据特定的数据反射获取该对象绑定的数据表结构对象参数
                var PropertyInfos = type.GetProperties().Where(o => o.PropertyType.Name == "DbSet`1").ToList();
                //构建数据库对象类型结构列表
                var LitModer = new List<WxTBType>();
                  //获取已记录的数据库表参数
                "TableProperties".GetStorage(ref LitModer);

                //判断数据库对象类型结构表是否为null
                if (LitModer == null)
                {
                    LitModer = new List<WxTBType>();
                }
                //构建新的数据库对象类型结构表
                var NewLitModer = new List<WxTBType>();

                foreach (var item in PropertyInfos) //循环取出反射得出的实体模型对象
                {
                    var Prion = item.PropertyType.GenericTypeArguments[0];

                    var forSQl = Prion.GetSql(); //获取实体对象的sql对象

                    NewLitModer.Add(forSQl);
                }
                
                if (LitModer.Count() <= 0)
                {
                    StringBuilder StrSql = new StringBuilder();

                    foreach (var item in NewLitModer)
                    {
                        StrSql.Append(item.SqlString);
                    }

                    this.Database.ExecuteSqlRaw(StrSql.ToString());
                    NewLitModer.ToStorage("TableProperties");
                }
                else
                {
                    var NewSql = new StringBuilder();

                    foreach (var item in NewLitModer)
                    {
                        var Model = LitModer.Where(o => o.ID == item.ID).ToList();


                        if (Model.Count() > 0)
                        {
                            var itemModel = Model.Single();

                            var IndexModel = LitModer.IndexOf(itemModel);

                            foreach (var NewPro in item.TableProperties)
                            {
                                var Pro = itemModel.TableProperties.Where(o => o.AttributName == NewPro.AttributName).ToList();
                                if (Pro.Count() > 0)
                                {
                                    var protype = Pro.First();

                                    if (protype.AttributTypeName != NewPro.AttributTypeName)
                                    {
                                        var index = LitModer[IndexModel].TableProperties.IndexOf(protype);

                                        LitModer[IndexModel].TableProperties[index].AttributTypeName = NewPro.AttributTypeName;

                                        NewSql.Append($"ALTER TABLE {item.TbName} MODIFY COLUMN {NewPro.AttributName} {NewPro.AttributTypeName} {NewPro.IsNot} COMMENT '{NewPro.Remark}' ;");
                                    }
                                }
                                else
                                {
                                    LitModer[IndexModel].TableProperties.Add(NewPro);
                                    NewSql.Append($"ALTER TABLE {item.TbName} ADD COLUMN {NewPro.AttributName} {NewPro.AttributTypeName} {NewPro.IsNot} COMMENT '{NewPro.Remark}';");
                                }
                            }

                            if (item.PrimaryKey != itemModel.PrimaryKey)
                            {
                                LitModer[IndexModel].PrimaryKey = item.PrimaryKey;

                                if (item.PrimaryKey.Length > 0)
                                {
                                    NewSql.Append($"ALTER TABLE {item.TbName} DROP PRIMARY KEY,ADD PRIMARY KEY({item.PrimaryKey}) USING BTREE; ");
                                }
                                else
                                {
                                    NewSql.Append($"ALTER TABLE {item.TbName} DROP PRIMARY KEY;");

                                }
                            }

                        }
                        else
                        {
                            LitModer.Add(item);
                            NewSql.Append(item.SqlString);
                        }

                    }

                    var strNewSql = NewSql.ToString();

                    if (strNewSql.Length > 2)
                    {
                        this.Database.ExecuteSqlRaw(strNewSql);
                    }
                    LitModer.ToStorage("TableProperties");
                }
            }
            catch (Exception er)
            {
                er.ErrRecord(typeof(WxBaseContent), 202);
                throw;
            }
        }
    }
}
