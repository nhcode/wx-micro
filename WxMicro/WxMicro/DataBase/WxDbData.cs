﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WxUtile.DataBase
{
    public class WxDbData
    {
        /// <summary>
        /// 标识数据库引擎，（sqlserver、mysql、sqlite、oracle）
        /// </summary>
        public string SQL { get; set; }

        /// <summary>
        /// 数据库连接字段
        /// </summary>
        public string DB { get; set; } 
    }
}
