﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WxUtile.Models;

namespace WxUtile.Interfaces
{
    /// <summary>
    /// 服务鉴权中间件对象
    /// </summary>
    public interface IAuthorizService
    {
       
        /// <summary>
        /// 通知用户消息
        /// </summary>
        /// <param name="Token"></param>
        /// <returns></returns>
        public void ToUser(IUser user);

        /// <summary>
        /// YTLCMISWebShopping
        ///  用户证书失效时间，单位分钟
        /// </summary>
        public long CertificateExpirationTime();

        /// <summary>
        /// 消息签名有效时间（单位：分钟）
        /// </summary>
        /// <returns></returns>
        public long MsgMinutes();

        /// <summary>
        /// 允许公共资源文件类型资源文件类型 ".png|.js|.jpg|.svg|.font"
        /// </summary>
        /// <returns></returns>
        public string GetPublicResourceTypes();

        /// <summary>
        /// 访问错误跳转页面
        /// </summary>
        /// <returns></returns>
        public WxError AccessError();

    }
}
