﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WxUtile.Attributes;
using WxUtile.Strings;
using WxUtile.Strings.Encrypts;

namespace WxUtile.Interfaces
{
    [Explain("用户资料对象")]
    public abstract class IUser
    {
        /// <summary>
        /// 用户唯一标识
        /// </summary>
        [NotNull]
        [Explain("用户唯一标识")]
        public abstract string ID { get; set; }

        /// <summary>
        /// 用户登录账号
        /// </summary>
        [NotNull]
        [Explain("用户登录账号")]
        public abstract string UserName { get; set; }

        /// <summary>
        /// 用户提供的RSA265-2048的公钥串
        /// </summary>

        [NotNull]
        [Explain("用户提供的RSA265-2048的公钥串")]
        public abstract string UserPublicKey { get; set; }

        /// <summary>
        /// 其他资料存储字符串
        /// </summary>
        [NotNull]
        [Explain("其他资料存储字符串")]
        protected abstract string OtherInformation { get; set; }

        /// <summary>
        /// 设置其他消息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        public void SetOtherInformation<T>(T t)
        {
            this.OtherInformation = t.ToJson().ToEnUnicode();
        }
        /// <summary>
        /// 获取用户允许使用连接授权接口ID数组
        /// </summary>
        /// <returns></returns>
        public abstract List<string> GetAuthPortIDArray();

        /// <summary>
        /// 其他资料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetOtherInformation<T>()
        {
            return this.OtherInformation.ToDnUnicode().FromJson<T>();
        }
    }
}
