﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WxUtile.Models;
using WxUtile.Configs;

namespace WxUtile.Interfaces
{
    /// <summary>
    /// 启动文件配置接口函数
    /// </summary>
    public interface IStartup
    {

        /// <summary>
        /// 获取用户证书对象
        /// </summary>
        /// <returns></returns>
        public WxTheISsuedBy GetTheISsuedBy();

        /// <summary>
        /// 获取Redis服务器连接地址
        /// </summary>
        /// <returns></returns>
        public string GetRedisConnecting();


        /// <summary>
        /// 数据初始化
        /// </summary>
        public void Initialize();

        /// <summary>
        /// 权限管理对象
        /// </summary>
        /// <returns></returns>
        public IAuthorizService GetAuthorizService();

    }


}
