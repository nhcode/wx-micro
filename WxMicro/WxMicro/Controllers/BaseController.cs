﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WxUtile.Attributes;
using WxUtile.Authorizs;
using WxUtile.Interfaces;
using WxUtile.Models;
using WxUtile.Models.Ports;
using WxUtile.Requests;
using WxUtile.Strings;
using WxUtile.Strings.Encrypts;

namespace WxUtile.Controllers
{
    /// <summary>
    /// 基础控制器对象（构建MVC控制器时需要继承该控制器）
    /// </summary>
    [EnableCors("allow_all")]
    public abstract class BaseController : Controller
    {
        /// <summary>
        /// 植入缓存服务器
        /// </summary>
        public CSRedis.CSRedisClient RedisClient = ConstantData.CSRedisClient;

        /// <summary>
        /// 颁发用户证书
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        protected WxCa Certificate(IUser user)
        {
            var ISsuedBy = ConstantData.ISsuedBy;

            if (ISsuedBy.TerracePrivateKey == "" || ISsuedBy.TerracePublicKey == "")
            {
                throw new Exception("平台公司公钥未设置，请在配置项{包名}Config.json中设置TerracePrivateKey与TerracePublicKey秘钥！");
            }
            WxRSA wxRSA = new  (1024);
            WxCertificate<IUser> Ca = new WxCertificate<IUser>
            {
                PublicKey = wxRSA.GetPublicKey(),
                PrivateKey = wxRSA.GetPrivateKey(),
                ExpiringDate = DateTime.Now.AddMinutes(ConstantData.CertificateExpirationTime),
                IssuanceDate = DateTime.Now,
                LoginIP=HttpContext.GetIP(),
                UserPublicKey=user.UserPublicKey,
                AuthPortIDs=user.GetAuthPortIDArray(),
                User =user
            };

            string Folder = ".\\Certificates\\";
            if (!System.IO.Directory.Exists(Folder))
            {
                System.IO.Directory.CreateDirectory(Folder);
            }

            string Path = $"{ Folder }{user.ID.GetMD5().ToUpper()}.ca";
            using (BinaryWriter writer = new BinaryWriter(System.IO.File.Open(Path, FileMode.Create)))
            { 
                var Data = Ca.ToJson();
                writer.Write(Data.AesEncrypt(ISsuedBy.SecurityKey));
            }

            return new WxCa
            {
                ID = user.ID,
                PublicKey = Ca.PublicKey
            };
        }
       
        /// <summary>
        /// 根据用户证书获取用户资料
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        protected bool GetUser(ref IUser User)
        {
            WxCertificate<WxUser> wxCertificate = null;
            string Msg = "";
           var isAuth= HttpContext.GetAuthorization(ref wxCertificate, ref Msg);
            User = isAuth ? wxCertificate.User : null;
            return isAuth;
        }

        /// <summary>
        /// 根据用户证书获取用户资料
        /// </summary>
        /// <param name="Token"></param>
        /// <param name="User">用户资料</param>
        /// <returns></returns>
        protected bool GetUser(string  Token,ref IUser User)
        {
            WxCertificate<WxUser> wxCertificate = null;
            string Msg = "";
           var isAuth= Token.GetAuthorization(HttpContext.GetIP(), ref wxCertificate, ref Msg);
            User = isAuth ? wxCertificate.User : null;
            return isAuth;
        }
       
        /// <summary>
        /// 获取接口列表
        /// </summary>
        /// <returns></returns>
        protected List<WxPort> GetWxPorts()
        {
            return PortServiceAttribute.Ports;
        }
    }
}
