﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WxUtile.Strings.Encrypts;

namespace WxUtile.Models.Ports
{
    /// <summary>
    /// 接口对象
    /// </summary>
    public class WxPort
    {
        /// <summary>
        /// 连接唯一编号
        /// </summary>
        public string ID { get =>Path.GetMD5().ToUpper(); }
        
        /// <summary>
        /// 模块名称
        /// </summary>
        public string ModuleName { get; set; }

        /// <summary>
        /// 接口连接访问路径
        /// </summary>
        public string Path { get; set; }
        
        /// <summary>
        /// 接口类型
        /// </summary>
        public string PathType { get; set; }

        /// <summary>
        /// 接口名称
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 请求方法
        /// </summary>
        public string RequesMethod { get; set; }

        /// <summary>
        /// 参数集合
        /// </summary>
        public List<WxParameter> Parameters { get; set; }

        /// <summary>
        /// 该接口是否需要授权
        /// </summary>
        public bool IsAuthorization { get; set; }

        /// <summary>
        /// 该接口是否需要用户资料
        /// </summary>
        public bool IsUserValidation { get; set; }

        /// <summary>
        /// 该接口是否需要签名处理（签名只支持RSA签名处理）
        /// </summary>
        public bool IsSign { get; set; }

        /// <summary>
        /// 该接口是否标识为菜单
        /// </summary>
        public bool IsMenu { get; set; }

        /// <summary>
        /// 该接口Token传输验证匿名在请求路径
        /// </summary>
        public bool IsUrlToken { get; set; }

        /// <summary>
        /// 菜单组名称（菜单组名称格式：用户组->权限组->授权组->....）
        /// </summary>
        public string MenuGroupName { get; set; }

        /// <summary>
        /// 菜单组对象
        /// </summary>
        public WxMenuGroup MenuGroup { get; set; }

        /// <summary>
        /// 菜单位置标识（用户可自行定义位置标识）
        /// </summary>
        public int MenuLocation { get; set; }

        /// <summary>
        /// 接口备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 子链接编号
        /// </summary>
        public List<string> SubchainIDs { get; set; }


    }
}
