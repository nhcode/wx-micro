﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WxUtile.Models.Ports
{
    /// <summary>
    /// 参数对象
    /// </summary>
    public class WxParameter
    {
        /// <summary>
        /// 参数名称
        /// </summary>
        public string ParameterName { get; set; }

        /// <summary>
        /// 参数类型
        /// </summary>
        public string ParameterType { get; set; }

        /// <summary>
        /// 参数说明
        /// </summary>
        public string ParameterSpecification { get; set; }

        /// <summary>
        /// 参数是否允许为空
        /// </summary>
        public bool IsNotNull { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
