﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WxUtile.Models.Ports
{
    /// <summary>
    /// 菜单组对象
    /// </summary>
    public  class WxMenuGroup 
    {
        /// <summary>
        /// 菜单组名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 子菜单组对象
        /// </summary>
        public   WxMenuGroup SubsetMenuGroup { get; set; }
    }
}
