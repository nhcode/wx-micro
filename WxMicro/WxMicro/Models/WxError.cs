﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WxUtile.Strings.Encrypts;

namespace WxUtile.Models
{
    /// <summary>
    /// 错误访问页面
    /// </summary>
    public class WxError
    {
        /// <summary>
        /// 页面错误访问地址
        /// </summary>
        public string PageErroUrl { get; set; }
        /// <summary>
        /// 接口错误访问地址
        /// </summary>
        public string APIErroUrl { get; set; }
    }
    /// <summary>
    /// 错误数据对象
    /// </summary>
    public class ErrorData
    {
        /// <summary>
        /// 错误代码
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 错误消息
        /// </summary>
        public string ErrorMsg { get; set; }

        /// <summary>
        /// 系统错误信息对象
        /// </summary>
        public string ExceptionJson { get; set; }

        /// <summary>
        /// 获取错误对象结构拼接字符串
        /// </summary>
        /// <returns></returns>
        public string GetData()
        {
            return $"?Code={Code}&ErrorMsg={ErrorMsg.ToUrlEncode()}&ExceptionJson={ExceptionJson.ToUrlEncode()}";
        }
    }
}
