﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WxUtile.Attributes;

namespace WxUtile.Models
{
    /// <summary>
    /// 证书签发对象
    /// </summary>
    public class WxTheISsuedBy
    {
        /// <summary>
        /// 平台数据加密安全秘钥，长度不得低于32位长度
        /// </summary>
        /// <returns></returns>
        public string SecurityKey { get; set; }

        /// <summary>
        /// 签发地址
        /// </summary>
        /// <returns></returns>
        public string ValidIssuer { get; set; }

        /// <summary>
        /// 服务数据请求地址
        /// </summary>
        public string ServiceURL { get; set; }

        /// <summary>
        /// 平台数字签名私钥
        /// </summary>
        public string TerracePrivateKey { get; set; }

        /// <summary>
        /// 平台数字签名验证公钥
        /// </summary>
        public string TerracePublicKey { get; set; }
    }
}
