﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WxUtile.Interfaces;

namespace WxUtile.Models
{
    /// <summary>
    /// 用户资料对象
    /// </summary>
    public class WxUser : IUser
    {
        /// <summary>
        /// 用户唯一标识
        /// </summary>
        public override string ID { get; set; }
        /// <summary>
        /// 用户登录账号
        /// </summary>
        public override string UserName { get; set; }
        /// <summary>
        /// 用户公钥
        /// </summary>
        public override string UserPublicKey { get; set ; }

        /// <summary>
        /// 用户其他资料
        /// </summary>
        protected override string OtherInformation { get; set; }
        /// <summary>
        /// 获取用户可以使用的接口列表
        /// </summary>
        /// <returns></returns>
        public override List<string> GetAuthPortIDArray()
        {
            return new List<string>();
        }
    }
}
